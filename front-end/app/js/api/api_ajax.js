//#region Namespace

//var $API = new Object();
// #endregion

function healthCheck() {
	alert("Health Check");
	$.ajax({
    	type: "GET",
        url: "/healthcheck",
        success: function (response) {                   
        	alert(JSON.stringify(response));
        },
        error: function (error) {
            alert(JSON.stringify(error));
        }
    });
}