angular.module( 'app', [ 'ui.router', 'constants', 'login', 'home', 'templates', 'ngMessages', 'ngMaterial',
  'LocalStorageModule', 'ngAutocomplete', 'api'
] ).config( function( $locationProvider, $urlRouterProvider, $httpProvider ) {
  $locationProvider.html5Mode( true );
  $urlRouterProvider.otherwise( '/home/dashboard' );
  $httpProvider.interceptors.push( 'SessionInjector' );
  $httpProvider.interceptors.push( 'ResponseInterceptor' );
} ).config( function( $mdThemingProvider ) {
  $mdThemingProvider.theme( 'default' ).primaryPalette( 'purple' ).accentPalette( 'grey' );
} ).run( function() {} );
