angular.module( 'constants', [] ).constant( 'Constants', {
  APP_URL: 'http://localhost:8090/api/v1/',
  // APP_URL: 'http://52.16.227.43:8080/hunter-0.0.1/api/v1/',
  AUTH_TOKEN_KEY: 'AUTH-TOKEN',
  USER_KEY: 'user',
  CHANNEL: {
    USER_LOGIN: 'user',
    SEARCH_ROW_SELECT: 'search_row_select',
    JOB_SEARCH_ROW_SELECT: 'job_search_row_select',
  }
} );
